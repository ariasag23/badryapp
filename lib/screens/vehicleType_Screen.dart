import 'dart:ui';

import 'package:badry/Widgets/my_Text_widgets.dart';
import 'package:badry/utils/app_colors.dart';
import 'package:badry/utils/size_config.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class VehicleType extends StatefulWidget {
  const VehicleType({Key? key}) : super(key: key);

  @override
  _VehicleTypeState createState() => _VehicleTypeState();
}

class _VehicleTypeState extends State<VehicleType> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.BACKGROUNDCOLOR_PAGE,
      body: Padding(
        padding: EdgeInsetsDirectional.only(
          start: SizeConfig.scaleWidth(78),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            SizedBox(
              height: SizeConfig.scaleHeight(107),
            ),
            Container(
                width: SizeConfig.scaleWidth(197),
                height: SizeConfig.scaleHeight(197),
                child: Image.asset('assets/images/logo.png')),
            SizedBox(
              height: SizeConfig.scaleHeight(48),
            ),
            ElevatedButton(
              onPressed: () {
                Navigator.pushReplacementNamed(context, '/gender_Screen');
              },
              child: MyText(
                text: 'قائد المركبة',
                color: AppColors.TEXT_COLOR,
                size: 18,
                fontWeight: FontWeight.w100,
              ),
              style: ElevatedButton.styleFrom(
                minimumSize: Size(
                    SizeConfig.scaleWidth(220), SizeConfig.scaleHeight(39)),
                onPrimary: AppColors.TEXT_COLOR,
                primary: Colors.transparent,
                side: BorderSide(
                    color: AppColors.TEXT_COLOR,
                    width: SizeConfig.scaleWidth(1)),
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(5)),
              ),
            ),
            SizedBox(
              height: SizeConfig.scaleHeight(30),
            ),
            ElevatedButton(
              onPressed: () {
                Navigator.pushReplacementNamed(context, '/gender_Screen');
              },
              child: MyText(
                text: 'عميل تجربة قيادة',
                color: AppColors.TEXT_COLOR,
                size: 18,
                fontWeight: FontWeight.w100,
              ),
              style: ElevatedButton.styleFrom(
                minimumSize: Size(
                    SizeConfig.scaleWidth(220), SizeConfig.scaleHeight(39)),
                onPrimary: AppColors.TEXT_COLOR,
                primary: Colors.transparent,
                side: BorderSide(
                    color: AppColors.TEXT_COLOR,
                    width: SizeConfig.scaleWidth(1)),
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(5)),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
