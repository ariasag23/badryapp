import 'package:badry/Widgets/my_Gender_Widgets.dart';
import 'package:badry/utils/app_colors.dart';
import 'package:badry/utils/size_config.dart';
import 'package:flutter/material.dart';

class Gender extends StatefulWidget {
  const Gender({Key? key}) : super(key: key);

  @override
  _GenderState createState() => _GenderState();
}

class _GenderState extends State<Gender> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.BACKGROUNDCOLOR_PAGE,
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0.0,
        actions: [
          IconButton(
              icon: Icon(
                Icons.arrow_forward_ios_outlined,
                color: AppColors.TEXT_COLOR,
              ),
              onPressed: () {
                Navigator.pushReplacementNamed(context, '/Sign_In_Screen');
              }),
        ],
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          SizedBox(
            height: SizeConfig.scaleHeight(107),
          ),
          Container(
              width: SizeConfig.scaleWidth(197),
              height: SizeConfig.scaleHeight(197),
              child: Image.asset('assets/images/logo.png')),
          SizedBox(
            height: SizeConfig.scaleHeight(48),
          ),
          Padding(
            padding: EdgeInsetsDirectional.only(
              start: SizeConfig.scaleWidth(85),
            ),
            child: Row(children: [
              MyGender(
                image: 'assets/images/boy.png',
                value: true,
                onTap: () {},
              ),
              SizedBox(
                width: SizeConfig.scaleWidth(16),
              ),
              MyGender(image: 'assets/images/girl.png'),
            ]),
          )
        ],
      ),
    );
  }
}
