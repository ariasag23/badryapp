import 'package:badry/Widgets/my_Text_Field.dart';
import 'package:badry/Widgets/my_Text_widgets.dart';
import 'package:badry/utils/app_colors.dart';
import 'package:badry/utils/size_config.dart';
import 'package:flutter/material.dart';

class SignUp extends StatefulWidget {
  const SignUp({Key? key}) : super(key: key);

  @override
  _SignUpState createState() => _SignUpState();
}

class _SignUpState extends State<SignUp> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.BACKGROUNDCOLOR_PAGE,
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0.0,
        leading: IconButton(
            icon: Icon(
              Icons.arrow_back_ios_outlined,
              color: AppColors.TEXT_COLOR,
            ),
            onPressed: () {
              Navigator.pop(context);
            }),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Container(
              width: SizeConfig.scaleWidth(124),
              height: SizeConfig.scaleHeight(124),
              child: Image.asset('assets/images/logo.png')),
          SizedBox(
            height: SizeConfig.scaleHeight(5),
          ),
          MyText(
            text: 'تسجيل جديد',
            size: 20,
            color: AppColors.COLOR_WHITE,
            fontWeight: FontWeight.bold,
          ),
          SizedBox(
            height: SizeConfig.scaleHeight(39),
          ),
          MyTextField(hint: 'الاسم'),
          SizedBox(
            height: SizeConfig.scaleHeight(25),
          ),
          MyTextField(hint: 'رقم الجوال'),
          SizedBox(
            height: SizeConfig.scaleHeight(25),
          ),
          MyTextField(hint: 'المدينة'),
          SizedBox(
            height: SizeConfig.scaleHeight(25),
          ),
          MyTextField(hint: 'الحي'),
          SizedBox(
            height: SizeConfig.scaleHeight(70),
          ),
          ElevatedButton(
            onPressed: () {
              Navigator.pushNamed(context, '/Pin_Code_Screen');
            },
            child: MyText(
              text: 'تسجيل',
              color: AppColors.COLOR_WHITE,
              size: 18,
              fontWeight: FontWeight.w100,
            ),
            style: ElevatedButton.styleFrom(
              minimumSize:
                  Size(SizeConfig.scaleWidth(343), SizeConfig.scaleHeight(47)),
              onPrimary: AppColors.TEXT_COLOR,
              primary: AppColors.TEXT_COLOR,
              side: BorderSide(
                  color: AppColors.TEXT_COLOR, width: SizeConfig.scaleWidth(1)),
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(5)),
            ),
          ),
        ],
      ),
    );
  }
}
