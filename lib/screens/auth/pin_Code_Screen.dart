import 'package:badry/Widgets/my_Text_widgets.dart';
import 'package:badry/utils/app_colors.dart';
import 'package:badry/utils/size_config.dart';
import 'package:pin_code_fields/pin_code_fields.dart';
import 'package:flutter/material.dart';

class PinCode extends StatefulWidget {
  const PinCode({Key? key}) : super(key: key);

  @override
  _PinCodeState createState() => _PinCodeState();
}

class _PinCodeState extends State<PinCode> {
  late FocusNode _firstFocusNode;
  late FocusNode _secodFocusNode;
  late FocusNode _thirdFocusNode;
  late FocusNode _fourthFocusNode;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _firstFocusNode = FocusNode();
    _secodFocusNode = FocusNode();
    _thirdFocusNode = FocusNode();
    _fourthFocusNode = FocusNode();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    _firstFocusNode.dispose();
    _secodFocusNode.dispose();
    _thirdFocusNode.dispose();
    _fourthFocusNode.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.BACKGROUNDCOLOR_PAGE,
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0.0,
        title: Center(
          child: MyText(
            text: 'رمز التحقق',
            color: AppColors.COLOR_WHITE,
            size: 18,
            fontWeight: FontWeight.bold,
            textAlign: TextAlign.center,
          ),
        ),
        leading: IconButton(
            icon: Icon(
              Icons.arrow_back_ios_outlined,
              color: AppColors.TEXT_COLOR,
            ),
            onPressed: () {
              Navigator.pop(context);
            }),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          SizedBox(
            height: SizeConfig.scaleHeight(30),
          ),
          MyText(
            text: 'سيصلك بعد قليل رسالة نصية  مكونة من 4 ارقام',
            size: 16,
            color: AppColors.COLOR_WHITE,
            fontWeight: FontWeight.w300,
          ),
          Padding(
            padding: EdgeInsets.symmetric(vertical: 40.0, horizontal: 57),
            child: PinCodeTextField(
              appContext: context,
              length: 4,
              obscureText: false,
              blinkWhenObscuring: true,
              animationType: AnimationType.scale,
              textStyle: TextStyle(color: AppColors.COLOR_WHITE),
              pinTheme: PinTheme(
                activeColor: AppColors.TEXT_COLOR,
                inactiveColor: AppColors.TEXT_COLOR,
                shape: PinCodeFieldShape.underline,
                selectedColor: AppColors.COLOR_WHITE,
              ),
              onChanged: (String text) {
                if (text.length == 1) {
                  _firstFocusNode.requestFocus();
                  _secodFocusNode.requestFocus();
                  _thirdFocusNode.requestFocus();
                  _fourthFocusNode.requestFocus();
                }
              },
            ),
          ),
          SizedBox(
            height: SizeConfig.scaleHeight(135),
          ),
          ElevatedButton(
            onPressed: () async {
              Navigator.popAndPushNamed(
                  context, '/Bottom_Navigation_MainScreen');
            },
            child: MyText(
              text: 'تأكيد',
              color: AppColors.COLOR_WHITE,
              size: 18,
              fontWeight: FontWeight.w100,
            ),
            style: ElevatedButton.styleFrom(
              minimumSize:
                  Size(SizeConfig.scaleWidth(343), SizeConfig.scaleHeight(47)),
              onPrimary: AppColors.TEXT_COLOR,
              primary: AppColors.TEXT_COLOR,
              side: BorderSide(
                  color: AppColors.TEXT_COLOR, width: SizeConfig.scaleWidth(1)),
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(5)),
            ),
          ),
        ],
      ),
    );
  }

  bool checkData() {
    return true;
  }
}
