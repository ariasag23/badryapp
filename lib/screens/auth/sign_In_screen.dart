import 'package:badry/Widgets/my_Text_Field.dart';
import 'package:badry/Widgets/my_Text_widgets.dart';
import 'package:badry/utils/app_colors.dart';
import 'package:badry/utils/size_config.dart';
import 'package:flutter/material.dart';

class SignIn extends StatefulWidget {
  const SignIn({Key? key}) : super(key: key);

  @override
  _SignInState createState() => _SignInState();
}

class _SignInState extends State<SignIn> {
  late TextEditingController _nameTextController;
  late TextEditingController _mobileTextController;

  void initState() {
    // TODO: implement initState
    super.initState();
    _nameTextController = TextEditingController();
    _mobileTextController = TextEditingController();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.BACKGROUNDCOLOR_PAGE,
      appBar: AppBar(
        elevation: 0.0,
        backgroundColor: AppColors.BACKGROUNDCOLOR_PAGE,
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Container(
              width: SizeConfig.scaleWidth(124),
              height: SizeConfig.scaleHeight(124),
              child: Image.asset('assets/images/logo.png')),
          SizedBox(
            height: SizeConfig.scaleHeight(5),
          ),
          MyText(
            text: 'تسجيل الدخول',
            size: 20,
            color: AppColors.COLOR_WHITE,
            fontWeight: FontWeight.bold,
          ),
          SizedBox(
            height: SizeConfig.scaleHeight(39),
          ),
          MyTextField(
            hint: 'الاسم',
            controller: _nameTextController,
          ),
          SizedBox(
            height: SizeConfig.scaleHeight(25),
          ),
          MyTextField(
            hint: 'رقم الجوال',
            controller: _mobileTextController,
          ),
          SizedBox(
            height: SizeConfig.scaleHeight(70),
          ),
          ElevatedButton(
            onPressed: () async {
              await performSave();
            },
            child: MyText(
              text: 'تسجيل دخول',
              color: AppColors.TEXT_COLOR,
              size: 18,
              fontWeight: FontWeight.w100,
            ),
            style: ElevatedButton.styleFrom(
              minimumSize:
                  Size(SizeConfig.scaleWidth(343), SizeConfig.scaleHeight(47)),
              onPrimary: AppColors.TEXT_COLOR,
              primary: Colors.transparent,
              side: BorderSide(
                  color: AppColors.TEXT_COLOR, width: SizeConfig.scaleWidth(1)),
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(5)),
            ),
          ),
          SizedBox(
            height: SizeConfig.scaleHeight(20),
          ),
          ElevatedButton(
            onPressed: () {
              Navigator.pushNamed(context, '/Sign_Up_Screen');
            },
            child: MyText(
              text: 'تسجيل جديد',
              color: AppColors.COLOR_WHITE,
              size: 18,
              fontWeight: FontWeight.w100,
            ),
            style: ElevatedButton.styleFrom(
              minimumSize:
                  Size(SizeConfig.scaleWidth(343), SizeConfig.scaleHeight(47)),
              onPrimary: AppColors.TEXT_COLOR,
              primary: AppColors.TEXT_COLOR,
              side: BorderSide(
                  color: AppColors.TEXT_COLOR, width: SizeConfig.scaleWidth(1)),
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(5)),
            ),
          ),
        ],
      ),
    );
  }

  Future performSave() async {
    if (checkData()) save();
  }

  bool checkData() {
    if (_nameTextController.text.isNotEmpty &&
        _mobileTextController.text.isNotEmpty) {
      return true;
    }

    return false;
  }

  Future save() async {
    // bool saved = await ContactDbController().create(contact);
  }

  void clear() {
    _nameTextController.text = '';
    _mobileTextController.text = '';
  }
}
