import 'package:badry/Widgets/Make%20_appointment.dart';
import 'package:badry/Widgets/appointment_Item.dart';
import 'package:badry/Widgets/my_Divider.dart';
import 'package:badry/Widgets/my_Text_widgets.dart';
import 'package:badry/Widgets/resevation_Details_Item.dart';
import 'package:badry/utils/app_colors.dart';
import 'package:badry/utils/size_config.dart';
import 'package:flutter/material.dart';

class ReservationDetails extends StatefulWidget {
  const ReservationDetails({Key? key}) : super(key: key);

  @override
  _ReservationDetailsState createState() => _ReservationDetailsState();
}

class _ReservationDetailsState extends State<ReservationDetails> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.BACKGROUNDCOLOR_PAGE,
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0.0,
        leading: IconButton(
            icon: Icon(
              Icons.arrow_back_ios_rounded,
              color: AppColors.TEXT_COLOR,
            ),
            onPressed: () {
              Navigator.pop(context);
            }),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 15),
          child:
              Column(crossAxisAlignment: CrossAxisAlignment.center, children: [
            Container(
              width: SizeConfig.scaleWidth(100),
              height: SizeConfig.scaleHeight(98),
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(10),
                image: DecorationImage(
                    image: AssetImage('assets/images/6.jpg'), fit: BoxFit.fill),
              ),
            ),
            SizedBox(
              height: SizeConfig.scaleHeight(13),
            ),
            MyText(
              text: 'محمد الحتو',
              size: 22,
              color: AppColors.COLOR_WHITE,
              fontWeight: FontWeight.bold,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Icon(
                  Icons.star,
                  color: AppColors.TEXT_COLOR,
                ),
                MyText(
                  text: '4.5',
                  color: AppColors.COLOR_WHITE,
                  size: 18,
                  fontWeight: FontWeight.w600,
                )
              ],
            ),
            SizedBox(
              height: SizeConfig.scaleHeight(5),
            ),
            Container(
              width: SizeConfig.scaleWidth(79),
              height: SizeConfig.scaleHeight(28),
              decoration: BoxDecoration(
                color: Color(0xFF707070),
                borderRadius: BorderRadius.circular(6),
              ),
              child: Center(
                child: MyText(
                  text: '200 ريال',
                  color: AppColors.COLOR_WHITE,
                  size: 17,
                  fontWeight: FontWeight.w300,
                ),
              ),
            ),
            MyDivider(
              color: AppColors.COLOR_WHITE,
              height: 1,
              indent: 30,
              endIndent: 30,
              heightSize: 10,
            ),
            SizedBox(
              height: SizeConfig.scaleHeight(15),
            ),
            Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
              ResevationDetailsItem(
                text1: 'مرسيدس',
                text2: 'نوع السيارة',
              ),
              SizedBox(
                height: SizeConfig.scaleHeight(10),
              ),
              ResevationDetailsItem(
                text1: '3 سنوات',
                text2: 'الخبرة',
              ),
              SizedBox(
                height: SizeConfig.scaleHeight(10),
              ),
              ResevationDetailsItem(
                text1: 'اوتوماتيك',
                text2: 'نوع الرخصة',
              ),
              SizedBox(
                height: SizeConfig.scaleHeight(10),
              ),
              ResevationDetailsItem(
                text1: 'النصر',
                text2: 'الحي',
              ),
            ]),
            SizedBox(
              height: SizeConfig.scaleHeight(20),
            ),
            Padding(
              padding: EdgeInsetsDirectional.only(
                end: SizeConfig.scaleWidth(10),
              ),
              child: Align(
                alignment: Alignment.centerRight,
                child: MyText(
                  text: 'احجز موعد',
                  size: 20,
                  color: AppColors.COLOR_WHITE,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            SizedBox(
              height: SizeConfig.scaleHeight(20),
            ),
            SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: Row(
                children: [
                  AppointmentItem(
                    name: 'الخميس',
                    color: AppColors.COLOR_WHITE,
                  ),
                  SizedBox(
                    width: SizeConfig.scaleWidth(5),
                  ),
                  AppointmentItem(
                    name: 'الاربعاء',
                    color: AppColors.COLOR_WHITE,
                  ),
                  SizedBox(
                    width: SizeConfig.scaleWidth(5),
                  ),
                  AppointmentItem(
                    name: 'الثلاثاء',
                    color: AppColors.COLOR_WHITE,
                  ),
                  SizedBox(
                    width: SizeConfig.scaleWidth(5),
                  ),
                  AppointmentItem(
                    name: 'الاثنين',
                    color: AppColors.COLOR_WHITE,
                  ),
                  SizedBox(
                    width: SizeConfig.scaleWidth(5),
                  ),
                  AppointmentItem(
                    name: 'الاحد',
                    color: AppColors.COLOR_WHITE,
                  ),
                  SizedBox(
                    width: SizeConfig.scaleWidth(5),
                  ),
                  AppointmentItem(
                    name: 'السبت',
                    color: AppColors.TEXT_COLOR,
                  ),
                ],
              ),
            ),
            SizedBox(
              height: SizeConfig.scaleHeight(20),
            ),
            Align(
              alignment: Alignment.centerRight,
              child: MyText(
                text: 'اختر الوقت',
                size: 20,
                color: AppColors.COLOR_WHITE,
                fontWeight: FontWeight.bold,
              ),
            ),
            SizedBox(
              height: SizeConfig.scaleHeight(15),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                MyText(
                  text: 'محجوز',
                  size: 16,
                  color: AppColors.COLOR_WHITE,
                  fontWeight: FontWeight.w400,
                ),
                SizedBox(
                  width: SizeConfig.scaleWidth(5),
                ),
                Container(
                  width: SizeConfig.scaleWidth(15),
                  height: SizeConfig.scaleHeight(15),
                  decoration: BoxDecoration(
                      color: AppColors.COLOR_Reserved, shape: BoxShape.circle),
                ),
                SizedBox(
                  width: SizeConfig.scaleWidth(15),
                ),
                MyText(
                  text: 'متاح للحجز',
                  size: 16,
                  color: AppColors.COLOR_WHITE,
                  fontWeight: FontWeight.w400,
                ),
                SizedBox(
                  width: SizeConfig.scaleWidth(5),
                ),
                Container(
                  width: SizeConfig.scaleWidth(15),
                  height: SizeConfig.scaleHeight(15),
                  decoration: BoxDecoration(
                      color: AppColors.COLOR_AVAILABLE, shape: BoxShape.circle),
                ),
              ],
            ),
            SizedBox(
              height: SizeConfig.scaleHeight(10),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                MakeAppointment(text: '11:00 - 10:00 ص'),
                MakeAppointment(text: '10:00 - 9:00 ص'),
                MakeAppointment(
                  text: '9:00 - 8:00 ص',
                  color: AppColors.COLOR_AVAILABLE,
                ),
              ],
            ),
            SizedBox(
              height: SizeConfig.scaleHeight(8),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                MakeAppointment(text: '2:00 - 1:00 م'),
                MakeAppointment(
                  text: '1:00 - 12:00 م',
                  color: AppColors.COLOR_Reserved,
                ),
                MakeAppointment(text: '12:00 - 11:00 ص'),
              ],
            ),
            SizedBox(
              height: SizeConfig.scaleHeight(8),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                MakeAppointment(
                  text: '3:00 - 2:00 م',
                  color: AppColors.COLOR_Reserved,
                ),
              ],
            ),
            SizedBox(
              height: SizeConfig.scaleHeight(20),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                MyText(
                  text: '200 ريال',
                  size: 16,
                  color: AppColors.TEXT_COLOR,
                  fontWeight: FontWeight.w600,
                ),
                SizedBox(
                  width: SizeConfig.scaleWidth(5),
                ),
                MyText(
                  text: ' : السعر الاجمالي',
                  size: 16,
                  color: AppColors.COLOR_WHITE,
                  fontWeight: FontWeight.bold,
                ),
              ],
            ),
            SizedBox(
              height: SizeConfig.scaleHeight(30),
            ),
            ElevatedButton(
              onPressed: () {
                //  Navigator.pushNamed(context, '/Sign_Up_Screen');
              },
              child: MyText(
                text: 'اخجز',
                color: AppColors.COLOR_WHITE,
                size: 18,
                fontWeight: FontWeight.w100,
              ),
              style: ElevatedButton.styleFrom(
                minimumSize: Size(
                    SizeConfig.scaleWidth(343), SizeConfig.scaleHeight(47)),
                onPrimary: AppColors.TEXT_COLOR,
                primary: AppColors.TEXT_COLOR,
                side: BorderSide(
                    color: AppColors.TEXT_COLOR,
                    width: SizeConfig.scaleWidth(1)),
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(5)),
              ),
            ),
            SizedBox(
              height: SizeConfig.scaleHeight(20),
            ),
          ]),
        ),
      ),
    );
  }
}
