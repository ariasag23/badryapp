import 'package:badry/Widgets/my_Text_widgets.dart';
import 'package:badry/utils/app_colors.dart';
import 'package:badry/utils/size_config.dart';
import 'package:flutter/material.dart';

class EndedScreen extends StatefulWidget {
  const EndedScreen({Key? key}) : super(key: key);

  @override
  _EndedScreenState createState() => _EndedScreenState();
}

class _EndedScreenState extends State<EndedScreen> {
  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 0.0,
      color: AppColors.COLOR_WHITE,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          MyText(
            text: 'اسم المدرب : احمد الحتو',
            size: 12,
            color: AppColors.BG_TEXT_FILELD,
            fontWeight: FontWeight.w700,
          ),
          SizedBox(
            height: SizeConfig.scaleHeight(6),
          ),
          MyText(
            text: 'اليوم : السبت',
            size: 12,
            color: AppColors.BG_TEXT_FILELD,
            fontWeight: FontWeight.w700,
          ),
          SizedBox(
            height: SizeConfig.scaleHeight(6),
          ),
          MyText(
            text: 'التاريخ : 2021 - 2 - 12',
            size: 12,
            color: AppColors.BG_TEXT_FILELD,
            fontWeight: FontWeight.w700,
          ),
          SizedBox(
            height: SizeConfig.scaleHeight(6),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              MyText(
                text: 'تم الانتهاء',
                size: 12,
                color: AppColors.COLOR_Reserved,
                fontWeight: FontWeight.w700,
              ),
              MyText(
                text: 'الوقت : 8:00 - 9:00 ص',
                size: 12,
                color: AppColors.BG_TEXT_FILELD,
                fontWeight: FontWeight.w700,
              ),
            ],
          ),
          SizedBox(
            height: SizeConfig.scaleHeight(21),
          ),
          Container(
            width: SizeConfig.scaleWidth(317),
            height: SizeConfig.scaleHeight(291),
            decoration: BoxDecoration(
                color: AppColors.COLOR_WHITE,
                borderRadius: BorderRadius.circular(15),
                border: Border.all(
                    color: AppColors.COLOR_AVAILABLE,
                    width: SizeConfig.scaleWidth(1)),
                boxShadow: [
                  BoxShadow(
                    color: Color(0xFF000000).withOpacity(0.5),
                    blurRadius: 1,
                  )
                ]),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SizedBox(
                  height: SizeConfig.scaleHeight(14),
                ),
                Container(
                  width: SizeConfig.scaleWidth(90),
                  height: SizeConfig.scaleHeight(88),
                  decoration: BoxDecoration(
                    color: Colors.black,
                    borderRadius: BorderRadius.circular(15),
                    image: DecorationImage(
                        image: AssetImage('assets/images/5.jpg'),
                        fit: BoxFit.fill),
                  ),
                ),
                SizedBox(
                  height: SizeConfig.scaleHeight(10),
                ),
                MyText(
                  text: 'إبراهيم شقورة',
                  size: 18,
                  color: AppColors.BG_TEXT_FILELD,
                  fontWeight: FontWeight.w700,
                ),
                SizedBox(
                  height: SizeConfig.scaleHeight(10),
                ),
                Container(
                  width: SizeConfig.scaleWidth(269),
                  height: SizeConfig.scaleHeight(73),
                  decoration: BoxDecoration(
                      color: AppColors.COLOR_CONTAINER_BOX,
                      boxShadow: [
                        BoxShadow(
                          color: Color(0xFF000000).withOpacity(0.5),
                        )
                      ]),
                  child: Center(
                    child: MyText(
                      text: 'ابراهيم طالب مجتهد',
                      size: 14,
                      color: AppColors.BG_TEXT_FILELD,
                      fontWeight: FontWeight.w700,
                    ),
                  ),
                ),
                SizedBox(
                  height: SizeConfig.scaleHeight(10),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Icon(
                      Icons.star,
                      color: AppColors.COLOR_AVAILABLE,
                    ),
                    MyText(
                      text: '4.5',
                      size: 14,
                      color: AppColors.BG_TEXT_FILELD,
                      fontWeight: FontWeight.w700,
                    ),
                  ],
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
