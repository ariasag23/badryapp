import 'package:badry/Widgets/my_Text_widgets.dart';
import 'package:badry/screens/bottom_navigation/bn_screens/pilgrims_Page_Screen/tabBar/ended_Screen.dart';
import 'package:badry/utils/app_colors.dart';
import 'package:flutter/material.dart';

class Pilgrims extends StatefulWidget {
  const Pilgrims({Key? key}) : super(key: key);

  @override
  _PilgrimsState createState() => _PilgrimsState();
}

class _PilgrimsState extends State<Pilgrims>
    with SingleTickerProviderStateMixin {
  late TabController _tabController;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _tabController = TabController(length: 2, vsync: this);
    _tabController.addListener(() {
      print('TAB BAR CONTROLLER LISTENER');
      print('LISTENER: Index(${_tabController.index})');
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.BACKGROUNDCOLOR_PAGE,
      appBar: AppBar(
        backgroundColor: AppColors.BACKGROUNDCOLOR_PAGE,
        elevation: 0.0,
        leading: Icon(
          Icons.arrow_back,
          color: AppColors.BACKGROUNDCOLOR_PAGE,
        ),
        title: Center(
          child: MyText(
            text: 'مشاويري',
            size: 18,
            color: AppColors.COLOR_WHITE,
            fontWeight: FontWeight.bold,
            textAlign: TextAlign.center,
          ),
        ),
      ),
      body: Container(
        height: double.infinity,
        width: double.infinity,
        decoration: BoxDecoration(
          color: AppColors.COLOR_WHITE,
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(20),
            topRight: Radius.circular(20),
          ),
        ),
        child: Padding(
          padding: EdgeInsets.symmetric(
            vertical: 13,
            horizontal: 21,
          ),
          child: Column(
            children: [
              TabBar(
                  controller: _tabController,
                  labelColor: AppColors.COLOR_AVAILABLE,
                  unselectedLabelColor: AppColors.BG_TEXT_FILELD,
                  indicatorColor: AppColors.COLOR_AVAILABLE,
                  tabs: [
                    Text(
                      'القادمة',
                      style:
                          TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                    ),
                    Text(
                      'المنتهية',
                      style:
                          TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                    ),
                  ]),
              Expanded(
                child: TabBarView(controller: _tabController, children: [
                  Center(
                    child: MyText(
                      text: 'لا يوجد',
                      size: 18,
                      color: Colors.black,
                    ),
                  ),
                  EndedScreen(),
                ]),
              )
            ],
          ),
        ),
      ),
    );
  }
}
