import 'package:badry/Widgets/my_Divider.dart';
import 'package:badry/Widgets/my_Text_widgets.dart';
import 'package:badry/Widgets/notification_Item.dart';
import 'package:badry/utils/app_colors.dart';
import 'package:badry/utils/size_config.dart';
import 'package:flutter/material.dart';

class NotificationScreen extends StatefulWidget {
  const NotificationScreen({Key? key}) : super(key: key);

  @override
  _NotificationState createState() => _NotificationState();
}

class _NotificationState extends State<NotificationScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: AppColors.BACKGROUNDCOLOR_PAGE,
        appBar: AppBar(
          elevation: 0.0,
          backgroundColor: AppColors.BACKGROUNDCOLOR_PAGE,
          leading: Icon(
            Icons.arrow_back,
            color: AppColors.BACKGROUNDCOLOR_PAGE,
          ),
          title: Center(
            child: MyText(
              text: 'الاشعارات',
              size: 24,
              fontWeight: FontWeight.bold,
              color: AppColors.COLOR_WHITE,
            ),
          ),
        ),
        body: Column(
          children: [
            NotificationItem(
              img: 'assets/images/6.jpg',
              nameUser: 'محمد الحتو',
              day: 'السبت',
              time: '9:00 - 8:00 ص',
              salary: '200 ريال',
            ),
            MyDivider(
              color: AppColors.COLOR_WHITE,
              height: 1,
              indent: 30,
              endIndent: 30,
              heightSize: 30,
            ),
            NotificationItem(
                img: 'assets/images/4.jpg',
                nameUser: 'احمد موسى',
                day: 'الاثنين',
                time: '12:00 - 11:00 ص',
                salary: '200 ريال'),
          ],
        ));
  }
}
