import 'package:badry/Widgets/my_Divider.dart';
import 'package:badry/Widgets/my_Text_widgets.dart';
import 'package:badry/utils/app_colors.dart';
import 'package:badry/utils/size_config.dart';
import 'package:flutter/material.dart';

class Profile extends StatefulWidget {
  const Profile({Key? key}) : super(key: key);

  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.COLOR_WHITE,
      appBar: AppBar(
        elevation: 0.0,
        backgroundColor: AppColors.COLOR_WHITE,
      ),
      body: SingleChildScrollView(
          child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 15),
        child: Column(crossAxisAlignment: CrossAxisAlignment.center, children: [
          Container(
            width: SizeConfig.scaleWidth(100),
            height: SizeConfig.scaleHeight(98),
            decoration: BoxDecoration(
              color: Colors.black,
              borderRadius: BorderRadius.circular(10),
              image: DecorationImage(
                  image: AssetImage('assets/images/5.jpg'), fit: BoxFit.fill),
            ),
          ),
          SizedBox(
            height: SizeConfig.scaleHeight(13),
          ),
          MyText(
            text: 'ابراهيم شقورة',
            size: 22,
            color: Colors.black,
            fontWeight: FontWeight.bold,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Icon(
                Icons.star,
                color: AppColors.TEXT_COLOR,
              ),
              MyText(
                text: '4.5',
                color: Colors.black,
                size: 18,
                fontWeight: FontWeight.w600,
              )
            ],
          ),
          MyDivider(
            color: Colors.black,
            height: 1,
            indent: 5,
            endIndent: 5,
            heightSize: 30,
          ),
          SizedBox(
            height: SizeConfig.scaleHeight(16),
          ),
          ListTile(
            leading: IconButton(
                icon: Icon(
                  Icons.keyboard_arrow_down_rounded,
                  size: 24,
                  color: Colors.black,
                ),
                onPressed: () {}),
            trailing: MyText(
              text: 'تعديل الحساب',
              size: 18,
              color: Colors.black,
              fontWeight: FontWeight.w800,
            ),
          ),
          ListTile(
            trailing: MyText(
              text: 'مشاويري',
              size: 18,
              color: Colors.black,
              fontWeight: FontWeight.w800,
            ),
          ),
          ListTile(
            leading: IconButton(
                icon: Icon(
                  Icons.keyboard_arrow_down_rounded,
                  size: 24,
                  color: Colors.black,
                ),
                onPressed: () {}),
            trailing: MyText(
              text: 'تقييماتي',
              size: 18,
              color: Colors.black,
              fontWeight: FontWeight.w800,
            ),
          ),
          ListTile(
            leading: IconButton(
                icon: Icon(
                  Icons.keyboard_arrow_down_rounded,
                  size: 24,
                  color: Colors.black,
                ),
                onPressed: () {}),
            trailing: MyText(
              text: 'البطاقة',
              size: 18,
              color: Colors.black,
              fontWeight: FontWeight.w800,
            ),
          ),
          ListTile(
            leading: IconButton(
                icon: Icon(
                  Icons.phone,
                  size: 24,
                  color: Colors.black,
                ),
                onPressed: () {}),
            trailing: MyText(
              text: 'تواصل معنا',
              size: 18,
              color: Colors.black,
              fontWeight: FontWeight.w800,
            ),
          ),
          ListTile(
            trailing: MyText(
              text: 'الشروط والأحكام',
              size: 18,
              color: Colors.black,
              fontWeight: FontWeight.w800,
            ),
          ),
          ListTile(
            trailing: MyText(
              text: 'تسجيل الخروج',
              size: 18,
              color: AppColors.COLOR_Reserved,
              fontWeight: FontWeight.w800,
            ),
          ),
        ]),
      )),
    );
  }
}
