import 'dart:collection';

import 'package:badry/Widgets/my_Text_widgets.dart';
import 'package:badry/provider/location_provider.dart';
import 'package:badry/utils/app_colors.dart';
import 'package:badry/utils/size_config.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:provider/provider.dart';

class GoogleMaps extends StatefulWidget {
  const GoogleMaps({Key? key}) : super(key: key);

  @override
  _GoogleMapsState createState() => _GoogleMapsState();
}

class _GoogleMapsState extends State<GoogleMaps> {
  var myMarkers = HashSet<Marker>();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Provider.of<LocationProvider>(context, listen: false).initalization();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<LocationProvider>(builder: (consumerContext, model, child) {
      // ignore: unnecessary_null_comparison
      if (model.lcationPosition != null) {
        return Stack(children: [
          GoogleMap(
            mapType: MapType.normal,
            myLocationButtonEnabled: false,
            myLocationEnabled: false,
            initialCameraPosition:
                CameraPosition(target: model.lcationPosition, zoom: 18),
            onMapCreated: (GoogleMapController controller) {
              setState(() {
                myMarkers.add(
                  Marker(
                      markerId: MarkerId('1'),
                      position: model.lcationPosition,
                      infoWindow: InfoWindow(
                        title: 'موقعي',
                      )),
                );
              });
            },
            markers: myMarkers,
          ),
          Padding(
            padding: EdgeInsetsDirectional.only(
                top: SizeConfig.scaleHeight(50),
                start: SizeConfig.scaleWidth(15),
                end: SizeConfig.scaleWidth(15)),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                InkWell(
                  onTap: () {
                    Navigator.pop(context);
                  },
                  child: Container(
                    width: SizeConfig.scaleWidth(40),
                    height: SizeConfig.scaleHeight(40),
                    decoration: BoxDecoration(
                        color: AppColors.COLOR_WHITE, shape: BoxShape.circle),
                    child: Center(
                      child: Icon(
                        Icons.arrow_back_ios,
                        color: Colors.black,
                      ),
                    ),
                  ),
                ),
                MyText(
                  text: 'حدد موقعك',
                  size: 18,
                  fontWeight: FontWeight.bold,
                ),
                InkWell(
                  onTap: () {
                    Navigator.pushReplacementNamed(context, '/Trainers_Screen');
                  },
                  child: Container(
                    width: SizeConfig.scaleWidth(40),
                    height: SizeConfig.scaleHeight(40),
                    decoration: BoxDecoration(
                        color: AppColors.COLOR_WHITE, shape: BoxShape.circle),
                    child: Center(
                      child: Icon(
                        Icons.send,
                        color: Colors.black,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: Container(
              height: SizeConfig.scaleHeight(115),
              width: double.infinity,
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(50),
                      topRight: Radius.circular(50)),
                  boxShadow: [
                    BoxShadow(
                      blurRadius: 19.0,
                    ),
                  ]),
              child: Padding(
                padding: EdgeInsetsDirectional.only(
                  start: SizeConfig.scaleWidth(15),
                  end: SizeConfig.scaleWidth(15),
                ),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Image.asset('assets/images/car.png'),
                    Spacer(),
                    MyText(
                      text: 'ابراهيم موسى',
                      size: 18,
                      fontWeight: FontWeight.bold,
                    ),
                    SizedBox(
                      width: SizeConfig.scaleWidth(10),
                    ),
                    Container(
                      width: SizeConfig.scaleWidth(41),
                      height: SizeConfig.scaleHeight(41),
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        color: AppColors.Person_Color,
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
        ]);
      }
      return Container(
        child: CircularProgressIndicator(),
      );
    });
  }
}
