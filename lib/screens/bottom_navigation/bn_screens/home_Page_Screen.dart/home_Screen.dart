import 'package:badry/screens/bottom_navigation/bn_screens/home_Page_Screen.dart/google_Maps_Screen.dart';
import 'package:badry/utils/app_colors.dart';

import 'package:flutter/material.dart';

class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  @override
  void initState() {
    // TODO: implement initState

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.BACKGROUNDCOLOR_PAGE,
      body: GoogleMaps(),
    );
  }
}
