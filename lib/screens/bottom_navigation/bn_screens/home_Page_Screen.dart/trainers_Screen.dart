import 'package:badry/Widgets/my_Divider.dart';
import 'package:badry/Widgets/my_Text_widgets.dart';
import 'package:badry/Widgets/trainers_Item.dart';
import 'package:badry/utils/app_colors.dart';
import 'package:badry/utils/size_config.dart';
import 'package:flutter/material.dart';

class Trainers extends StatefulWidget {
  const Trainers({Key? key}) : super(key: key);

  @override
  _TrainersState createState() => _TrainersState();
}

class _TrainersState extends State<Trainers> {
  late bool online;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.BACKGROUNDCOLOR_PAGE,
      appBar: AppBar(
        elevation: 0.0,
        backgroundColor: Colors.transparent,
        title: Center(
          child: MyText(
            text: 'أسماء المدربين الأقرب',
            size: 18,
            fontWeight: FontWeight.bold,
            color: AppColors.COLOR_WHITE,
            textAlign: TextAlign.center,
          ),
        ),
        leading: IconButton(
            icon: Icon(
              Icons.arrow_back_ios_rounded,
              color: AppColors.TEXT_COLOR,
            ),
            onPressed: () {
              Navigator.pop(context);
            }),
        actions: [],
      ),
      body: Padding(
        padding: EdgeInsets.symmetric(vertical: 20, horizontal: 15),
        child: Column(children: [
          TrainersItem(
            image: 'assets/images/6.jpg',
            minutes: '5 دقائق',
            reservation: '12 حجز',
            car: 'مرسيدس',
            name: 'محمد الحتو',
            assess: '4.5',
            price: '200 ريال',
            press: () {
              Navigator.pushNamed(context, '/reservation_Details');
            },
          ),
          MyDivider(
              color: AppColors.COLOR_DIVIDER,
              height: 1,
              indent: 30,
              endIndent: 30),
          SizedBox(
            height: SizeConfig.scaleHeight(5),
          ),
          TrainersItem(
            image: 'assets/images/3.jpg',
            minutes: '7 دقائق',
            reservation: '4 حجز',
            car: 'بي ام',
            name: 'احمد موسى',
            assess: '4.2',
            price: '200 ريال',
          ),
          MyDivider(
              color: AppColors.COLOR_DIVIDER,
              height: 1,
              indent: 30,
              endIndent: 30),
          SizedBox(
            height: SizeConfig.scaleHeight(5),
          ),
          TrainersItem(
            image: 'assets/images/4.jpg',
            minutes: '3 دقائق',
            reservation: '2 حجز',
            car: 'سكودا',
            name: 'علي مهدى',
            assess: '3.5',
            price: '200 ريال',
          ),
        ]),
      ),
    );
  }
}
