import 'package:badry/models/bn_screen.dart';
import 'package:badry/screens/bottom_navigation/bn_screens/home_Page_Screen.dart/home_Screen.dart';

import 'package:badry/screens/bottom_navigation/bn_screens/notifications_Screen.dart';
import 'package:badry/screens/bottom_navigation/bn_screens/pilgrims_Page_Screen/pilgrims_Screen.dart';

import 'package:badry/screens/bottom_navigation/bn_screens/profile_Screen.dart';

import 'package:badry/utils/app_colors.dart';
import 'package:flutter/material.dart';

class BottomNavigationMainScreen extends StatefulWidget {
  const BottomNavigationMainScreen({Key? key}) : super(key: key);

  @override
  _BottomNavigationMainScreenState createState() =>
      _BottomNavigationMainScreenState();
}

class _BottomNavigationMainScreenState
    extends State<BottomNavigationMainScreen> {
  int _currentIndex = 0;
  List<BnScreen> _bnScreens = <BnScreen>[
    const BnScreen('home', Home()),
    const BnScreen('notification', NotificationScreen()),
    const BnScreen('pilgrims', Pilgrims()),
    const BnScreen('Profile', Profile()),
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.transparent,
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: _currentIndex,
        onTap: (int selectedItemIndex) {
          setState(() {
            _currentIndex = selectedItemIndex;
          });
        },
        backgroundColor: Colors.black,
        type: BottomNavigationBarType.fixed,
        selectedIconTheme: IconThemeData(color: AppColors.TEXT_COLOR),
        selectedItemColor: AppColors.TEXT_COLOR,
        unselectedIconTheme:
            IconThemeData(color: AppColors.Unselected_Icon_Theme),
        unselectedItemColor: AppColors.Unselected_Icon_Theme,
        items: [
          BottomNavigationBarItem(
            icon: Icon(Icons.location_on),
            label: '',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.notifications),
            label: '',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.car_repair),
            label: '',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.person),
            label: '',
          ),
        ],
      ),
      body: _getScreen(),
    );
  }

  Widget _getScreen() => _bnScreens.elementAt(_currentIndex).screen;

  String _getScreenTitle() => _bnScreens.elementAt(_currentIndex).title;
}
