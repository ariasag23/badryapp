import 'package:badry/Widgets/my_Text_widgets.dart';
import 'package:badry/utils/app_colors.dart';
import 'package:badry/utils/size_config.dart';
import 'package:flutter/material.dart';

class PaymentScreen extends StatefulWidget {
  const PaymentScreen({Key? key}) : super(key: key);

  @override
  _PushScreenState createState() => _PushScreenState();
}

class _PushScreenState extends State<PaymentScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.BACKGROUNDCOLOR_PAGE,
      appBar: AppBar(
        backgroundColor: AppColors.BACKGROUNDCOLOR_PAGE,
        elevation: 0.0,
        title: Center(
            child: MyText(
          text: 'الدفع',
          size: 18,
          fontWeight: FontWeight.bold,
          color: AppColors.COLOR_WHITE,
        )),
        leading: IconButton(
            icon: Icon(
              Icons.arrow_back_ios_rounded,
              color: AppColors.TEXT_COLOR,
            ),
            onPressed: () {
              Navigator.pop(context);
            }),
        actions: [],
      ),
      body: Padding(
        padding: EdgeInsets.symmetric(vertical: 17, horizontal: 17),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.end,
          children: [
            Image.asset('assets/images/push.png'),
            Container(
              width: SizeConfig.scaleWidth(342),
              height: SizeConfig.scaleHeight(250),
              decoration: BoxDecoration(
                color: Colors.transparent,
                borderRadius: BorderRadius.circular(5),
                border: Border.all(
                    color: AppColors.TEXT_COLOR,
                    width: SizeConfig.scaleWidth(1)),
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  SizedBox(
                    height: SizeConfig.scaleHeight(5),
                  ),
                  Container(
                    width: SizeConfig.scaleWidth(104),
                    height: SizeConfig.scaleHeight(27),
                    child: Image.asset('assets/images/push.png'),
                  ),
                  SizedBox(
                    height: SizeConfig.scaleHeight(20),
                  ),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      MyText(
                        text: 'تاريخ الانتهاء',
                        size: 14,
                        fontWeight: FontWeight.w700,
                        color: AppColors.COLOR_WHITE,
                      ),
                      MyText(
                        text: 'رقم البطاقة',
                        size: 14,
                        fontWeight: FontWeight.w700,
                        color: AppColors.COLOR_WHITE,
                      ),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Container(
                        width: SizeConfig.scaleWidth(109),
                        height: SizeConfig.scaleHeight(33),
                        decoration: BoxDecoration(
                          color: AppColors.COLOR_WHITE,
                          borderRadius: BorderRadius.circular(5),
                        ),
                        child: Padding(
                          padding: EdgeInsets.symmetric(horizontal: 5),
                          child: TextField(
                            textAlign: TextAlign.center,
                            keyboardType: TextInputType.number,
                            decoration: InputDecoration(
                              hintText: 'XX/XX',
                              enabledBorder: null,
                              focusedBorder: UnderlineInputBorder(
                                borderSide: BorderSide(
                                    color: Colors.white.withOpacity(0.01),
                                    width: SizeConfig.scaleWidth(0)),
                              ),
                            ),
                          ),
                        ),
                      ),
                      Container(
                        width: SizeConfig.scaleWidth(165),
                        height: SizeConfig.scaleHeight(33),
                        decoration: BoxDecoration(
                          color: AppColors.COLOR_WHITE,
                          borderRadius: BorderRadius.circular(5),
                        ),
                        child: Padding(
                          padding: EdgeInsets.symmetric(horizontal: 5),
                          child: TextField(
                            keyboardType: TextInputType.number,
                            decoration: InputDecoration(
                              hintText: '2345  xxxx  xxxx  xxxx',
                              enabledBorder: null,
                              focusedBorder: UnderlineInputBorder(
                                borderSide: BorderSide(
                                    color: Colors.white.withOpacity(0.01),
                                    width: SizeConfig.scaleWidth(0)),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                  //////////////////////////////////////////////////
                  SizedBox(
                    height: SizeConfig.scaleHeight(20),
                  ),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      MyText(
                        text: 'CVV',
                        size: 14,
                        fontWeight: FontWeight.w700,
                        color: AppColors.COLOR_WHITE,
                      ),
                      MyText(
                        text: 'حامل البطاقة',
                        size: 14,
                        fontWeight: FontWeight.w700,
                        color: AppColors.COLOR_WHITE,
                      ),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Container(
                        width: SizeConfig.scaleWidth(109),
                        height: SizeConfig.scaleHeight(33),
                        decoration: BoxDecoration(
                          color: AppColors.COLOR_WHITE,
                          borderRadius: BorderRadius.circular(5),
                        ),
                        child: TextField(
                          textAlign: TextAlign.center,
                          keyboardType: TextInputType.number,
                          decoration: InputDecoration(
                            hintText: 'CVV',
                            enabledBorder: null,
                            focusedBorder: UnderlineInputBorder(
                              borderSide: BorderSide(
                                  color: Colors.white.withOpacity(0.01),
                                  width: SizeConfig.scaleWidth(0)),
                            ),
                          ),
                        ),
                      ),
                      Container(
                        width: SizeConfig.scaleWidth(165),
                        height: SizeConfig.scaleHeight(33),
                        decoration: BoxDecoration(
                          color: AppColors.COLOR_WHITE,
                          borderRadius: BorderRadius.circular(5),
                        ),
                        child: TextField(
                          textAlign: TextAlign.center,
                          keyboardType: TextInputType.emailAddress,
                          decoration: InputDecoration(
                            hintText: 'حامل البطاقة',
                            enabledBorder: null,
                            focusedBorder: UnderlineInputBorder(
                              borderSide: BorderSide(
                                  color: Colors.white.withOpacity(0.01),
                                  width: SizeConfig.scaleWidth(0)),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            /////
            SizedBox(
              height: SizeConfig.scaleHeight(30),
            ),
            Align(
              alignment: Alignment.center,
              child: ElevatedButton(
                onPressed: () {
                  // Navigator.pushNamed(context, '/Sign_Up_Screen');
                },
                child: MyText(
                  text: 'دفع',
                  color: AppColors.COLOR_WHITE,
                  size: 18,
                  fontWeight: FontWeight.w100,
                ),
                style: ElevatedButton.styleFrom(
                  minimumSize: Size(
                      SizeConfig.scaleWidth(154), SizeConfig.scaleHeight(47)),
                  onPrimary: AppColors.TEXT_COLOR,
                  primary: AppColors.TEXT_COLOR,
                  side: BorderSide(
                      color: AppColors.TEXT_COLOR,
                      width: SizeConfig.scaleWidth(1)),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(5)),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
