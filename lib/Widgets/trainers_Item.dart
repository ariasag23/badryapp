import 'package:badry/Widgets/my_Text_widgets.dart';
import 'package:badry/utils/app_colors.dart';
import 'package:badry/utils/size_config.dart';
import 'package:flutter/material.dart';

class TrainersItem extends StatelessWidget {
  final String minutes;
  final String reservation;
  final String car;
  final String name;
  final String assess;
  final String image;
  final String price;
  final VoidCallback? press;

  TrainersItem(
      {required this.minutes,
      required this.reservation,
      required this.car,
      required this.name,
      required this.assess,
      required this.image,
      required this.price,
      this.press});

  @override
  Widget build(BuildContext context) {
    return Column(children: [
      InkWell(
        onTap: press,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Expanded(
              flex: 0,
              child: Column(
                children: [
                  MyText(
                    text: minutes,
                    size: 16,
                    color: AppColors.COLOR_WHITE,
                    fontWeight: FontWeight.w300,
                  ),
                  Image.asset('assets/images/car.png'),
                ],
              ),
            ),
            Expanded(
              flex: 1,
              child: Column(
                children: [
                  MyText(
                    text: reservation,
                    size: 16,
                    color: AppColors.COLOR_WHITE,
                    fontWeight: FontWeight.w300,
                  ),
                  MyText(
                    text: car,
                    size: 16,
                    color: AppColors.COLOR_WHITE,
                    fontWeight: FontWeight.w300,
                  ),
                ],
              ),
            ),
            Column(
              children: [
                MyText(
                  text: name,
                  size: 16,
                  color: AppColors.COLOR_WHITE,
                  fontWeight: FontWeight.w300,
                ),
                Row(
                  children: [
                    Icon(
                      Icons.star,
                      size: 12,
                      color: AppColors.TEXT_COLOR,
                    ),
                    MyText(
                      text: assess,
                      size: 16,
                      color: AppColors.COLOR_WHITE,
                      fontWeight: FontWeight.w300,
                    ),
                  ],
                )
              ],
            ),
            SizedBox(
              width: 10,
            ),
            Container(
              width: SizeConfig.scaleWidth(55),
              height: SizeConfig.scaleHeight(54),
              decoration: BoxDecoration(
                color: AppColors.COLOR_WHITE,
                borderRadius: BorderRadius.circular(10),
                image:
                    DecorationImage(image: AssetImage(image), fit: BoxFit.fill),
              ),
            ),
          ],
        ),
      ),
      SizedBox(
        height: SizeConfig.scaleHeight(15),
      ),
      MyText(
        text: 'السعر : ${price}',
        size: 18,
        color: AppColors.TEXT_COLOR,
        fontWeight: FontWeight.bold,
      ),
    ]);
  }
}
