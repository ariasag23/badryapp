import 'package:badry/utils/app_colors.dart';
import 'package:badry/utils/size_config.dart';
import 'package:flutter/material.dart';

class MyDivider extends StatelessWidget {
  final Color color;
  final double height;
  final double indent;
  final double endIndent;
  final double heightSize;

  MyDivider(
      {required this.color,
      required this.height,
      required this.indent,
      required this.endIndent,
      this.heightSize = 5});

  @override
  Widget build(BuildContext context) {
    return SizedBox(
        height: SizeConfig.scaleHeight(heightSize),
        child: Divider(
          color: color,
          height: height,
          indent: indent,
          endIndent: endIndent,
        ));
  }
}
