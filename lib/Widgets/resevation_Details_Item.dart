import 'package:badry/Widgets/my_Text_widgets.dart';
import 'package:badry/utils/app_colors.dart';
import 'package:badry/utils/size_config.dart';
import 'package:flutter/material.dart';

class ResevationDetailsItem extends StatelessWidget {
  final String text1;
  final String text2;

  ResevationDetailsItem({
    required this.text1,
    required this.text2,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsetsDirectional.only(
        start: SizeConfig.scaleHeight(20),
        end: SizeConfig.scaleHeight(20),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          MyText(
            text: text1,
            size: 18,
            color: AppColors.TEXT_COLOR,
            fontWeight: FontWeight.w600,
          ),
          MyText(
            text: text2,
            size: 18,
            color: AppColors.COLOR_WHITE,
            fontWeight: FontWeight.w600,
          ),
        ],
      ),
    );
  }
}
