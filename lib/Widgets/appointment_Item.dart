import 'package:badry/Widgets/my_Text_widgets.dart';
import 'package:badry/utils/app_colors.dart';
import 'package:badry/utils/size_config.dart';
import 'package:flutter/material.dart';

class AppointmentItem extends StatelessWidget {
  final String name;
  final Color color;

  AppointmentItem({required this.name, required this.color});
  @override
  Widget build(BuildContext context) {
    return Container(
      width: SizeConfig.scaleWidth(81),
      height: SizeConfig.scaleHeight(42),
      decoration: BoxDecoration(
        color: Colors.transparent,
        borderRadius: BorderRadius.circular(10),
        border: Border.all(color: color, width: 1),
      ),
      child: Center(
          child: MyText(
        text: name,
        size: 16,
        color: AppColors.COLOR_WHITE,
        fontWeight: FontWeight.w300,
      )),
    );
  }
}
