import 'package:badry/Widgets/my_Text_widgets.dart';
import 'package:badry/utils/app_colors.dart';
import 'package:badry/utils/size_config.dart';
import 'package:flutter/material.dart';

class NotificationItem extends StatelessWidget {
  final String img;
  final String nameUser;
  final String day;
  final String time;
  final String salary;

  NotificationItem({
    required this.img,
    required this.nameUser,
    required this.day,
    required this.time,
    required this.salary,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 15),
      child: Column(
        children: [
          SizedBox(
            height: SizeConfig.scaleHeight(10),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              MyText(
                text: 'تم قبول طلبك',
                size: 16,
                color: AppColors.COLOR_WHITE,
                fontWeight: FontWeight.w700,
              ),
              SizedBox(
                width: SizeConfig.scaleWidth(10),
              ),
              Icon(
                Icons.notifications_rounded,
                color: AppColors.TEXT_COLOR,
              ),
            ],
          ),
          SizedBox(
            height: SizeConfig.scaleHeight(10),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              ElevatedButton(
                onPressed: () {
                  Navigator.pushReplacementNamed(context, '/push_Screen');
                },
                child: MyText(
                  text: 'دفع',
                  color: AppColors.COLOR_WHITE,
                  size: 18,
                  fontWeight: FontWeight.w100,
                ),
                style: ElevatedButton.styleFrom(
                  minimumSize: Size(
                      SizeConfig.scaleWidth(154), SizeConfig.scaleHeight(47)),
                  onPrimary: AppColors.TEXT_COLOR,
                  primary: AppColors.TEXT_COLOR,
                  side: BorderSide(
                      color: AppColors.TEXT_COLOR,
                      width: SizeConfig.scaleWidth(1)),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(5)),
                ),
              ),
              SizedBox(
                width: SizeConfig.scaleWidth(14),
              ),
              Container(
                width: SizeConfig.scaleWidth(128),
                height: SizeConfig.scaleWidth(183),
                decoration: BoxDecoration(
                  color: AppColors.BACKGROUNDCOLOR_PAGE,
                  borderRadius: BorderRadius.circular(15),
                  border: Border.all(
                      color: AppColors.COLOR_WHITE,
                      width: SizeConfig.scaleWidth(1)),
                ),
                child: Padding(
                  padding: EdgeInsets.symmetric(
                    horizontal: 10,
                    vertical: 14,
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Container(
                        width: SizeConfig.scaleWidth(57),
                        height: SizeConfig.scaleHeight(56),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(15),
                          image: DecorationImage(
                              image: AssetImage(img), fit: BoxFit.fill),
                        ),
                      ),
                      SizedBox(
                        height: SizeConfig.scaleHeight(5),
                      ),
                      MyText(
                        // ignore: unnecessary_brace_in_string_interps
                        text: 'المدرب:  ${nameUser}',
                        size: 11,
                        fontWeight: FontWeight.w700,
                        color: AppColors.COLOR_WHITE,
                      ),
                      SizedBox(
                        height: SizeConfig.scaleHeight(6),
                      ),
                      MyText(
                        // ignore: unnecessary_brace_in_string_interps
                        text: 'اليوم: ${day}',
                        size: 11,
                        fontWeight: FontWeight.w700,
                        color: AppColors.COLOR_WHITE,
                      ),
                      SizedBox(
                        height: SizeConfig.scaleHeight(6),
                      ),
                      MyText(
                        // ignore: unnecessary_brace_in_string_interps
                        text: 'الوقت: ${time}',
                        size: 9,
                        fontWeight: FontWeight.w700,
                        color: AppColors.COLOR_WHITE,
                      ),
                      SizedBox(
                        height: SizeConfig.scaleHeight(6),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          MyText(
                            // ignore: unnecessary_brace_in_string_interps
                            text: '${salary}',
                            size: 11,
                            fontWeight: FontWeight.w700,
                            color: AppColors.TEXT_COLOR,
                          ),
                          MyText(
                            // ignore: unnecessary_brace_in_string_interps
                            text: ' : السعر',
                            size: 11,
                            fontWeight: FontWeight.w700,
                            color: AppColors.COLOR_WHITE,
                          )
                        ],
                      )
                    ],
                  ),
                ),
              ),
            ],
          )
        ],
      ),
    );
  }
}
