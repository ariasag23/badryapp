import 'package:badry/Widgets/my_Text_widgets.dart';
import 'package:badry/utils/app_colors.dart';
import 'package:badry/utils/size_config.dart';
import 'package:flutter/material.dart';

class MakeAppointment extends StatelessWidget {
  final String text;
  final Color color;

  MakeAppointment({
    required this.text,
    this.color = AppColors.COLOR_WHITE,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 8),
      child: Container(
        width: SizeConfig.scaleWidth(98),
        height: SizeConfig.scaleHeight(42),
        decoration: BoxDecoration(
          color: Colors.transparent,
          borderRadius: BorderRadius.circular(5),
          border: Border.all(color: color, width: SizeConfig.scaleWidth(1)),
        ),
        child: Center(
          child: MyText(
            text: text,
            size: 14,
            color: AppColors.COLOR_WHITE,
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
    );
  }
}
