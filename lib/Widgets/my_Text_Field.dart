import 'package:badry/utils/app_colors.dart';
import 'package:badry/utils/size_config.dart';
import 'package:flutter/material.dart';

class MyTextField extends StatelessWidget {
  final String hint;
  final TextInputType textInputType;
  final bool obscureText;
  final TextEditingController? controller;
  final int maxLength;
  final String? suffixText;

  MyTextField({
    required this.hint,
    this.suffixText,
    this.textInputType = TextInputType.text,
    this.obscureText = false,
    this.maxLength = 45,
    this.controller,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsetsDirectional.only(
        start: SizeConfig.scaleWidth(16),
        end: SizeConfig.scaleWidth(16),
      ),
      child: TextField(
        textAlign: TextAlign.right,
        keyboardType: textInputType,
        maxLines: 1,
        obscureText: obscureText,
        controller: controller,
        maxLength: maxLength,
        decoration: InputDecoration(
          hintText: hint,
          fillColor: AppColors.BG_TEXT_FILELD,
          filled: true,
          counterText: '',
          contentPadding: EdgeInsets.symmetric(
            horizontal: SizeConfig.scaleWidth(25),
            vertical: SizeConfig.scaleHeight(16),
          ),
          hintStyle: TextStyle(
            color: AppColors.COLOR_WHITE,
            fontFamily: 'SF Pro Display',
            fontSize: SizeConfig.scaleTextFont(15),
          ),
          // suffix: Text('Forgot'),
          suffixIcon: suffixText != null
              ? InkWell(
                  onTap: () {
                    print('ON TAP : SUFFIX');
                  },
                  child: Container(
                    width: SizeConfig.scaleWidth(51),
                    margin: EdgeInsetsDirectional.only(
                      end: SizeConfig.scaleWidth(23),
                    ),
                    alignment: AlignmentDirectional.centerEnd,
                    child: Text(
                      'Forgot?',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        color: AppColors.SUFFIX_TEXT_FIELD_COLOR,
                        fontSize: SizeConfig.scaleTextFont(15),
                      ),
                    ),
                  ),
                )
              : null,
          suffixStyle: TextStyle(color: AppColors.SUFFIX_TEXT_FIELD_COLOR),
          enabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(5),
              borderSide: BorderSide(color: Colors.transparent)),
          focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(5),
              borderSide: BorderSide(color: Colors.transparent)),
        ),
      ),
    );
  }
}
