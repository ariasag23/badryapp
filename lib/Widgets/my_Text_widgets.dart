import 'package:badry/utils/size_config.dart';
import 'package:flutter/material.dart';

class MyText extends StatelessWidget {
  final String text;
  final Color color;
  final double size;
  final FontWeight fontWeight;
  final String fontFamily;
  final TextAlign textAlign;

  MyText(
      {required this.text,
      this.color = Colors.black,
      this.size = 16,
      this.fontWeight = FontWeight.normal,
      this.fontFamily = '',
      this.textAlign = TextAlign.center});

  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      style: TextStyle(
          color: color,
          fontSize: SizeConfig.scaleTextFont(size),
          fontWeight: fontWeight,
          fontFamily: fontFamily),
      textAlign: textAlign,
    );
  }
}
