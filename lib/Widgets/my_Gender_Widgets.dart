import 'package:badry/utils/app_colors.dart';
import 'package:badry/utils/size_config.dart';
import 'package:flutter/material.dart';

class MyGender extends StatelessWidget {
  final String image;
  final bool? value;
  final Color color;
  final VoidCallback? onTap;

  MyGender(
      {required this.image,
      this.value,
      this.color = Colors.transparent,
      this.onTap});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Container(
        width: SizeConfig.scaleWidth(90),
        height: SizeConfig.scaleHeight(90),
        decoration: BoxDecoration(
          color: value == true ? AppColors.TEXT_COLOR : Colors.transparent,
          borderRadius: BorderRadius.circular(15),
          border: Border.all(
              color: AppColors.TEXT_COLOR, width: SizeConfig.scaleWidth(1)),
        ),
        child: Image.asset(image),
      ),
    );
  }
}
