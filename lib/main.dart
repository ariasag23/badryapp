import 'package:badry/provider/location_provider.dart';
import 'package:badry/screens/D%D9%92etails/reservation_Details.dart';
import 'package:badry/screens/auth/pin_Code_Screen.dart';
import 'package:badry/screens/auth/sign_In_screen.dart';
import 'package:badry/screens/auth/sign_Up_Screen.dart';
import 'package:badry/screens/bottom_navigation/bn_screens/home_Page_Screen.dart/trainers_Screen.dart';

import 'package:badry/screens/bottom_navigation/bottom_navigation_main_screen.dart';
import 'package:badry/screens/gender_Screen.dart';
import 'package:badry/screens/launch_screen.dart';
import 'package:badry/screens/payment_Screen.dart';

import 'package:badry/screens/vehicleType_Screen.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(
          create: (_) => LocationProvider(),
        )
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        // home: TestScreen(),
        initialRoute: '/launch_screen',
        routes: {
          // Screens
          '/launch_screen': (context) => LaunchScreen(),
          '/Vehicle_Type': (context) => VehicleType(),
          '/gender_Screen': (context) => Gender(),
          '/push_Screen': (context) => PaymentScreen(),
          // Details
          '/reservation_Details': (context) => ReservationDetails(),

          // auth
          '/Sign_In_Screen': (context) => SignIn(),
          '/Sign_Up_Screen': (context) => SignUp(),
          '/Pin_Code_Screen': (context) => PinCode(),

          // BottomNavigationMainScreen

          '/Bottom_Navigation_MainScreen': (context) =>
              BottomNavigationMainScreen(),

          // Home Page Screen
          '/Trainers_Screen': (context) => Trainers(),
        },
      ),
    );
  }
}
