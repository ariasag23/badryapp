import 'package:flutter/material.dart';

class AppColors {
  static const BACKGROUNDCOLOR_PAGE = Color(0xFF2E2D2C);
  static const TEXT_COLOR = Color(0xFFFECD02);
  static const COLOR_WHITE = Color(0xFFFFFFFF);
  static const BG_TEXT_FILELD = Color(0xFF000000);
  static const Unselected_Icon_Theme = Color(0xFF707070);
  static const Person_Color = Color(0xFFD1D1D1);
  static const COLOR_DIVIDER = Color(0xFF707070);
  static const COLOR_AVAILABLE = Color(0xFFFECD02);
  static const COLOR_Reserved = Color(0xFFFF0000);
  static const COLOR_CONTAINER_BOX = Color(0xFFF2F1F1);

  ///
  static const GRADIENT_END_COLOR = Color(0xFF21223D);
  static const GRADIENT_START_COLOR = Color(0xFF464765);
  static const GREY_COLOR = Color(0xFFB4BBC9);
  static const TEXT_FIELD_BG = Color(0xFF2D2E45);
  static const SUFFIX_TEXT_FIELD_COLOR = Color(0xFFEAEAEE);
  static const RICH_TEXT_COLOR = Color(0xFF6E6F86);
  static const BUTTON_GRADIENT_START_COLOR = Color(0xFF857DDA);
  static const BUTTON_GRADIENT_END_COLOR = Color(0xFF5E4FFC);
  static const DIVIDER_COLOR = Color(0xFF4B5063);
  static const OR_DIVIDER_COLOR = Color(0xFFA3A4B3);
  static const BOTTOM_NAVIGATION_COLOR = Color(0xFF3F4164);
  static const ONLINE_COLOR = Color(0xFF05DB99);
  static const OFFLINE_COLOR = Color(0xFFF56868);
  static const LAST_MESSAGE_COLOR = Color(0xFF6E6F86);
  static const SELECTED_INDICATOR_COLOR = Color(0xFF514BFF);
  static const UN_SELECTED_INDICATOR_COLOR = Color(0xFFD6E1FF);
  static const PAGE_VIEW_NEXT_COLOR = Color(0xFF5C4DF7);

  static const BOTTOM_NAVIGATION_SELECTED_ICON_COLOR = Color(0xFF5C4DF7);
  static const BOTTOM_NAVIGATION_UN_SELECTED_ICON_COLOR = Color(0xFF6B6D93);
  static const BOTTOM_NAVIGATION_SELECTED_LABEL_COLOR = Color(0xFFEAEAEE);
  static const BOTTOM_NAVIGATION_UN_SELECTED_LABEL_COLOR = Color(0xFF6E6F86);
}
